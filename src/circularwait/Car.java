package circularwait;

public class Car {
    private Status status = Status.WASHED;
    private int WASH_IN_MILLI = 1000;
    private int POLISH_IN_MILLI = 2000;

    public Car(Status status) {
        this.status = status;
    }

    public synchronized void wash() throws InterruptedException {
        if (status == Status.WASHED) {
            wait();
        }

        System.out.println(String.format("Washing the car, (would take %d milliseconds by %s).", WASH_IN_MILLI, Thread.currentThread().getName()));
        Thread.sleep(WASH_IN_MILLI);
        status = Status.WASHED;
        System.out.println("Car is washed.");

        notify();
    }

    public synchronized void polish() throws InterruptedException {
        if (status == Status.POLISHED) {
            wait();
        }

        System.out.println(String.format("Polishing the car, (would take %d milliseconds by %s).", POLISH_IN_MILLI, Thread.currentThread().getName()));
        Thread.sleep(POLISH_IN_MILLI);
        status = Status.POLISHED;
        System.out.println("Car is polished.");

        notify();
    }

    public Status getStatus() {
        return status;
    }

}

enum Status {
    WASHED, POLISHED
}