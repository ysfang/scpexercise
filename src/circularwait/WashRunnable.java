package circularwait;

public class WashRunnable implements Runnable {

    private Car car;

    public WashRunnable(Car car) {
        this.car = car;
    }

    @Override
    public void run() {
        try {
            while (true) {
                car.wash();
            }
        } catch (InterruptedException e) {
            System.out.println("WashRunnable is interrupted.");
        }
    }
}
