package circularwait;

public class PolishRunnable implements Runnable {

    private Car car;

    public PolishRunnable(Car car) {
        this.car = car;
    }

    @Override
    public void run() {
        try {
            while (true) {
                car.polish();
            }
        } catch (InterruptedException e) {
            System.out.println("PolishRunnable is interrupted.");
        }
    }
}
