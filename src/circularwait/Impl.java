package circularwait;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Impl {
    private static int RUN_IN_SECOND = 10;

    public static void main(String[] args) throws InterruptedException {
        Car car = new Car(Status.POLISHED);
        System.out.println("Generated a car with status: " + car.getStatus());

        List<Runnable> carServiceList = Arrays.asList(new WashRunnable(car), new PolishRunnable(car));
        ExecutorService executor = Executors.newFixedThreadPool(carServiceList.size());
        for (Runnable service: carServiceList) {
            executor.execute(service);
        }

        executor.shutdown();
        if (!executor.awaitTermination(RUN_IN_SECOND, TimeUnit.SECONDS)) {
            executor.shutdownNow();
        }
    }
}
