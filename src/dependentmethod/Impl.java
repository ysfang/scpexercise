package dependentmethod;

import dependentmethod.service.CalculateService;
import dependentmethod.service.CalculateServiceF;
import dependentmethod.service.CalculateServiceG;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Impl {
    public static void main(String[] args) {
        List<CalculateService> liService = new ArrayList<>();
        liService.add(new CalculateServiceF());
        liService.add(new CalculateServiceG());

        ExecutorService executor = Executors.newFixedThreadPool(liService.size());
        for (CalculateService service: liService) {
            executor.submit(service);
        }

        executor.shutdown();
    }
}

