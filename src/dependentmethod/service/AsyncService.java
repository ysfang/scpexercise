package dependentmethod.service;

import dependentmethod.MultiKeyMapUtil;
import dependentmethod.entity.TransactionEntiry;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.collections4.map.MultiKeyMap;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AsyncService {
    public static List<TransactionEntiry> calculateAndSum(List<CalculateService> liService) {
        ExecutorService executor = Executors.newFixedThreadPool(liService.size());
        List<Future<List<TransactionEntiry>>> list = new ArrayList();

        for (CalculateService service: liService) {
            list.add(executor.submit(service));
        }

        MultiKeyMap mapResult = MultiKeyMap.multiKeyMap(new LinkedMap());
        for(Future<List<TransactionEntiry>> future : list){
            MultiKeyMap mapTarget = null;
            try {
                mapTarget = MultiKeyMapUtil.toMultiKeyMap(future.get());
                mapResult = MultiKeyMapUtil.add(mapResult, mapTarget);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        executor.shutdown();
        return MultiKeyMapUtil.toList(mapResult);
    }
}
