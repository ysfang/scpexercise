package dependentmethod.service;

import dependentmethod.entity.TransactionEntiry;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class CalculateServiceE implements CalculateService {
    @Override
    public List<TransactionEntiry> calculate() {
        return Arrays.asList(new TransactionEntiry("A", "B", "E", BigDecimal.valueOf(1)));
    }

    @Override
    public List<TransactionEntiry> call() throws Exception {
        int WAIT_IN_MILLI = 3000;
        System.out.println("Service E would take " + WAIT_IN_MILLI + " milliseconds.");
        Thread.sleep(WAIT_IN_MILLI);
        return calculate();
    }
}
