package dependentmethod.service;

import dependentmethod.entity.TransactionEntiry;

import java.util.ArrayList;
import java.util.List;

import static dependentmethod.MultiKeyMapUtil.printList;

public class CalculateServiceF implements CalculateService {
    @Override
    public List<TransactionEntiry> calculate() {
        List<CalculateService> liService = new ArrayList<>();
        liService.add(new CalculateServiceA());
        liService.add(new CalculateServiceB());

        List<TransactionEntiry> result = AsyncService.calculateAndSum(liService);
        printList("Invoker Service F Result as below:", result);
        return result;
    }

    @Override
    public List<TransactionEntiry> call() throws Exception {
        System.out.println("Invoker Service F running...");
        return calculate();
    }
}
