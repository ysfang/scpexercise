package dependentmethod.service;

import dependentmethod.entity.TransactionEntiry;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class CalculateServiceA implements CalculateService {
    @Override
    public List<TransactionEntiry> calculate() {
        return Arrays.asList(new TransactionEntiry("A", "B", "C", BigDecimal.ONE),
                new TransactionEntiry("A", "B", "D", BigDecimal.valueOf(2)));
    }

    @Override
    public List<TransactionEntiry> call() throws Exception {
        int WAIT_IN_MILLI = 1000;
        System.out.println("Service A would take " + WAIT_IN_MILLI + " milliseconds.");
        Thread.sleep(WAIT_IN_MILLI);
        return calculate();
    }
}
