package dependentmethod.service;

import dependentmethod.entity.TransactionEntiry;

import java.util.List;
import java.util.concurrent.Callable;

public interface CalculateService extends Callable<List<TransactionEntiry>> {
    List<TransactionEntiry> calculate();
}
