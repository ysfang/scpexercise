package dependentmethod.service;

import dependentmethod.entity.TransactionEntiry;

import java.util.ArrayList;
import java.util.List;

import static dependentmethod.MultiKeyMapUtil.printList;

public class CalculateServiceG implements CalculateService {
    @Override
    public List<TransactionEntiry> calculate() {
        List<CalculateService> liService = new ArrayList<>();
        liService.add(new CalculateServiceB());
        liService.add(new CalculateServiceD());
        liService.add(new CalculateServiceE());

        List<TransactionEntiry> result = AsyncService.calculateAndSum(liService);
        printList("Invoker Service G Result as below:", result);

        return result;
    }

    @Override
    public List<TransactionEntiry> call() throws Exception {
        System.out.println("Invoker Service G running...");
        return calculate();
    }
}
