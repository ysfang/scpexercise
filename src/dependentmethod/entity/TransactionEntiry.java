package dependentmethod.entity;

import java.math.BigDecimal;

public class TransactionEntiry {
    private String clientLayout;
    private String billingCode;
    private String batchNumber;
    private BigDecimal count;

    public TransactionEntiry(String clientLayout, String billingCode, String batchNumber,
            BigDecimal count) {
        this.clientLayout = clientLayout;
        this.billingCode = billingCode;
        this.batchNumber = batchNumber;
        this.count = count;
    }

    public String getClientLayout() {
        return clientLayout;
    }

    public void setClientLayout(String clientLayout) {
        this.clientLayout = clientLayout;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }
}
