package dependentmethod;

import dependentmethod.entity.TransactionEntiry;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.collections4.map.MultiKeyMap;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MultiKeyMapUtil {
    public static MultiKeyMap toMultiKeyMap(List<TransactionEntiry> li) {
        MultiKeyMap map = MultiKeyMap.multiKeyMap(new LinkedMap());
        li.forEach((it) -> {
            map.put(it.getClientLayout(), it.getBillingCode(), it.getBatchNumber(), it.getCount());
        });
        return map;
    }

    public static MultiKeyMap add(MultiKeyMap mainMap, MultiKeyMap targetMap) {
        targetMap.forEach((k, v) -> {
            MultiKey mk = (MultiKey) k;
            if (mainMap.containsKey(mk.getKey(0), mk.getKey(1), mk.getKey(2))) {
                mainMap.put(mk.getKey(0), mk.getKey(1), mk.getKey(2),
                        ((BigDecimal) v).add((BigDecimal) mainMap.get(mk.getKey(0), mk.getKey(1), mk.getKey(2))));
            } else {
                mainMap.put(mk.getKey(0), mk.getKey(1), mk.getKey(2), v);
            }
        });

        return mainMap;
    }

    public static List<TransactionEntiry> toList(MultiKeyMap<MultiKey, BigDecimal> map) {
        List<TransactionEntiry> li = new ArrayList<>();
        map.forEach((k, v) -> {
            MultiKey mk = k;
            li.add(new TransactionEntiry((String) mk.getKey(0), (String) mk.getKey(1), (String) mk.getKey(2), v));
        });

        return li;
    }

    public static void printList(String prefix, List<TransactionEntiry> li) {
        System.out.println(prefix);
        li.forEach(e ->{
            DecimalFormat df = new DecimalFormat();
            System.out.println(String.format("[\"%s\", \"%s\", \"%s\", %s]", e.getClientLayout(), e.getBillingCode(), e.getBatchNumber(), df.format(e.getCount())));
        });
    }
}
